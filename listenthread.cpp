#include "listenthread.h"

using namespace std;

ListenThread::ListenThread() {
    cur_client = 0;
}

ListenThread::~ListenThread() {
    closesocket(clientSocket);
    closesocket(listenSocket);
}

int ListenThread::setup() {
    WSADATA dat;
    if(WSAStartup(MAKEWORD(2,2),&dat)!=0)
    {
        cout << "Init Falied: " << GetLastError() << endl;
        system("pause");
        return -1;
    }

    listenSocket=socket(PF_INET,SOCK_STREAM,IPPROTO_TCP);
    if(listenSocket==INVALID_SOCKET)
    {
        cout << "Creating Listen Socket Failed: " << GetLastError() << endl;
        system("pause");
        return -1;
    }

    listenAddr.sin_family = PF_INET;
    listenAddr.sin_port = htons(PORT);
    listenAddr.sin_addr.S_un.S_addr = INADDR_ANY;

    int namelen = sizeof (listenAddr);
    const struct sockaddr* name = (LPSOCKADDR)&listenAddr;
    int result = bind(listenSocket, name, namelen);

    if(result == SOCKET_ERROR) {
        cout << "Bind Error: " << GetLastError() << endl;
        system("pause");
        return -1;
    }

    if(listen(listenSocket, 5) == SOCKET_ERROR) {
        cout << "Listen Error: " << GetLastError() << endl;
        system("pause");
        return -1;
    }
    return 0;
}

void ListenThread::run() {
    QString ip; //IP подключающегося пользователя
    string b1, b2, b3, b4, b0; //Октеты IP адреса
    setup(); //Настройка сокета для прослушки
    int remoteAddrLen = sizeof(remoteAddr);
    while(true) {
        if(!isRunning) return;
        if(cur_client<max_client) {
            clientSocket = accept(listenSocket, (SOCKADDR *)&remoteAddr, &remoteAddrLen); //Принятие запроса на подключение
            if(clientSocket == INVALID_SOCKET) {
                if(!isRunning) return;
                cout << "Create client socket error!" << endl;
                continue;
            }
            cur_client++;
            b1 = to_string(remoteAddr.sin_addr.S_un.S_un_b.s_b1); //Получение IP адреса клиента
            b2 = to_string(remoteAddr.sin_addr.S_un.S_un_b.s_b2);
            b3 = to_string(remoteAddr.sin_addr.S_un.S_un_b.s_b3);
            b4 = to_string(remoteAddr.sin_addr.S_un.S_un_b.s_b4);
            b0 = b1 + "." + b2 + "." + b3 + "." + b4;
            ip = QString::fromStdString(b0);
            emit emitSocket(clientSocket, ip); //Добавление клиента в список в UI
        }
        Sleep(100);
    }
}

void ListenThread::stop() {
    closesocket(clientSocket);
    closesocket(listenSocket);
    WSACleanup();
    quit();
}
