#ifndef CUSTOMTHREAD_H
#define CUSTOMTHREAD_H

#include "server.h"
#include <windows.h>
#include <conio.h>
#include <iostream>
#include <vector>
#include <process.h>
#include <string>

class customThread : public QObject
{
    Q_OBJECT
public:
    bool isRunning = false;
    static CRITICAL_SECTION critSection;
    HANDLE thread;
    bool createThread();
    void start();
    void stop();
    static unsigned int __stdcall staticRun(void* data);
    virtual void run() {};
    void quit();
    void wait();


    explicit customThread(QObject *parent = nullptr);
    ~customThread();

};

#endif // CUSTOMTHREAD_H
