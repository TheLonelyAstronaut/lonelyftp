#include "customthread.h"
#include <iostream>
using namespace std;

customThread::customThread(QObject *parent){
    //InitializeCriticalSection(&(customThread::critSection));
}

customThread::~customThread(){
   // DeleteCriticalSection(&(customThread::critSection));
}


bool customThread::createThread(){
    thread = (HANDLE)_beginthreadex(
            NULL,                   // Pointer to a SECURITY_ATTRIBUTES structure that determines whether the returned handle can be inherited by child processes.
            0,                      // Stack size for a new thread, or 0.
            &customThread::staticRun,       // Start address of a routine that begins execution of a new thread.
            this,          // Argument list to be passed to a new thread, or NULL.
            0,                      // Flags that control the initial state of a new thread.
            NULL);   // Points to a 32-bit variable that receives the thread identifier. If it's NULL, it's not used.

    cout <<  GetLastError() << endl;

    if(thread == NULL){
        ExitProcess(3);
        return false;
    }
    return true;
}

void customThread::start(){
    isRunning = true;
    createThread();
    //EnterCriticalSection(&critSection);
}

void customThread::stop(){
    wait();
    quit();
}

void customThread::quit(){
   //unnecessary because in our override function run thread have already terminated
   //ExitThread(3);
   //LeaveCriticalSection(&critSection);
    isRunning = false;
    wait();
}

unsigned int __stdcall customThread::staticRun(void* data){
    //std::cout << "customThread::run(): Internal error, this implementation should never be called." << std::endl;
    ((customThread*)data)->run();
}

void customThread::wait(){
    WaitForSingleObject(thread, INFINITE);
    TerminateThread(thread, 0);
}











