#ifndef LISTENTHREAD_H
#define LISTENTHREAD_H

#include <WinSock2.h>
#include <windows.h>
#include <iostream>
#include <QString>
#include <string>
#include "customthread.h"

class ListenThread : public customThread
{
    Q_OBJECT
public:
    ListenThread();
    ~ListenThread();

    int setup();
    void stop();

    int max_client;
    int cur_client;

protected:
    void run();

private:
    bool stopped;
    SOCKET listenSocket;
    SOCKET clientSocket;
    SOCKADDR_IN listenAddr;
    SOCKADDR_IN remoteAddr;

private slots:

signals:
    void emitSocket(SOCKET, QString);
    void deleteInstance(QString);
};

#endif // LISTENTHREAD_H
